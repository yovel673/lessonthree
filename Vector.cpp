#include "Vector.h"
#include <iostream>

/*
The class c'tor, gets n(the resize factor and capacity
*/
Vector::Vector(int n)
{
	if (n < 2) //if n less than 2, 2 will be defult value
		n = 2;
	_size = 0;
	_elements = new int[n];
	_capacity = n;
	_resizeFactor = n;
}

/*
class d'tor
*/
Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}
int Vector::size() const                       /********************************************/
{											   /*  *  *  *  *  *  *                        */
	return _size;							   /*   *  *  *  *  *     **********************/
}											   /*  *  *  *  *  *  *                        */
int Vector::capacity() const				   /*   *  *  *  *  *     **********************/
{											   /*  *  *  *  *  *  *                        */
	return _capacity;						   /*    *  *  *  *  *    **********************/
}											   /*  *  *  *  *  *  *                        */
int Vector::resizeFactor() const			   /********************************************/
{											   /*              class setters               */
	return _resizeFactor;					   /********************************************/
}											   /*                                          */
bool Vector::empty() const					   /********************************************/
{											   /*       GOD BLESS AMERICA                  */
	return _size;							   /********************************************/
}


void Vector::push_back(const int& val)//adds element at the end of the vector
{
	int n = 0;
	if (_capacity <= _size)
	{
		n = _capacity + _resizeFactor;
		reserve(n);
	}
	else
	{
		n = _size;
		_size++;
		_elements[n] = val;
	}
}
int Vector::pop_back()//removes and returns the last element of the vector
{
	int element = 0;
	if (_size == 0)
		element = 0;
	else
	{
		element = _elements[_size - 1];
		_elements[_size - 1] = 0;
		_size--;
	}
	return element;
}
void Vector::reserve(int n)//change the capacity
{
	int newfactors = 0;
	int *newarr = NULL;
	int i = 0;
	if (n < 0)
	{
		std::cout << "error!! cant put negative number for n";
	}
	else if (n > _capacity)
	{
		newfactors = (n - _capacity) / _resizeFactor;
		if ((n - _capacity) % _resizeFactor > 0)
			newfactors++;
		
		_capacity = (newfactors * _resizeFactor) +_capacity;
		newarr = new int[_capacity];
		for (i = 0; i < _size; i++)
		{
			newarr[i] = _elements[i];
		}
		delete[] _elements;
		_elements = newarr;
	}

}
void Vector::resize(int n)//change _size to n, unless n is greater than the vector's capacity
{
	int* newarr = NULL;
	int i = 0;
	if (n <= _capacity)
	{
		newarr = new int[n];
		for (i = 0; i <n; i++)
		{
			newarr[i] = _elements[i];
		}
		delete[] _elements;
		_elements = newarr;
		_capacity = n;
		_size = n;
	}
	else if (_capacity < n)
	{
		reserve(n);
	}
}
void Vector::assign(int val)//assigns val to all elemnts
{
	int i = 0;
	for (i = 0; i < _size; i++)
	{
		_elements[i] = val;
	}
}
void Vector::resize(int n, const int& val)//same as above, if new elements added their value is val
{
	int i = 0;
	if (n <= _capacity)
	{
		resize(n);
	}
	else
	{
		reserve(n);
		for (i = _size-1; i < _capacity; i++)
		{
			_elements[i] = val;
		}
		_size = _capacity;
	}
}

//C

Vector::Vector(const Vector& other) //Copy c'tor, creats new object simillr to given object with deep copy
{
	*this = other;
}


//D
//Element Access
int& Vector::operator[](int n) const//n'th element
{
	if (n >= _size || n < 0)
	{
		std::cout << "error no such index!";
		n = 0;
	}
	return _elements[n];
}

Vector& Vector::operator=(const Vector& n) //the big three
{
	int i = 0;

	_capacity = n._capacity;
	_size = n._size;
	_resizeFactor = n._resizeFactor;
	_elements = new int[_capacity];
	for (i = 0; i < _size; i++)
	{
		_elements[i] = n._elements[i];
	}
	return *this;
}
/*
Printing the Vector
*/
void Vector::printVector()
{
	int i = 0;
	std::cout << "Vector capcity:" << _capacity<<"\n";
	std::cout << "Vector size:" << _size << "\n";
	std::cout << "Vector resize factor:" << _resizeFactor << "\n";

	for (i = 0; i < _size; i++)
	{
		std::cout << _elements[i] << "\n";
	}
}
